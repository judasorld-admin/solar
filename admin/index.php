<?php
    session_start();
    include_once('../config/config.php');
    include_once('../config/createTable.php');

    // logout
    if(isset($_GET['logout']) && $_GET['logout']==1){
        unset($_SESSION['Admin_access']);
        header('Location: index.php');
    }

    if(isset($_POST['accessKey']) && !empty($_POST['accessKey'])){
        $key = $_POST['accessKey'];

        if($key == '12345'){
            $_SESSION['Admin_access'] = 1;
        }else{
            unset($_SESSION['Admin_access']);
            $msg = 'Invalid Access!';
        }
    }

    // update pitch
    if(isset($_POST['pitch_id']) && isset($_POST['pitch_name']) && isset($_POST['pitch_value']) && isset($_POST['updatePitch'])){
        $id = $_POST['pitch_id'];
        $name = $_POST['pitch_name'];
        $value = $_POST['pitch_value'];

        $sql = "UPDATE pitch SET name='".$name."' , value='".$value."' where id= ".$id.";";     
        $update = $conn->query($sql);
        if($update){
            $update_msg = "Updated";
        }else{
            $update_msg = "Could not update!";
        }
        echo($update_msg); exit;
    }

    // update system
    if(isset($_POST['system_id']) && isset($_POST['system_name']) && isset($_POST['system_value']) && isset($_POST['updateSystem'])){
        $id = $_POST['system_id'];
        $name = $_POST['system_name'];
        $value = $_POST['system_value'];

        $sql = "UPDATE system SET name='".$name."' , value='".$value."' where id= ".$id.";";            
        $update = $conn->query($sql);
        if($update){
            $update_msg = "Updated";
        }else{
            $update_msg = "Could not update!";
        }
        echo($update_msg); exit;
    }

    // update solar
    if(isset($_POST['solar_value'])){
        $value = $_POST['solar_value'];

        $sql = "UPDATE solar SET capacity='".$value."' ;";            
        $update = $conn->query($sql);
        if($update){
            $update_msg = "Updated";
        }else{
            $update_msg = "Could not update!";
        }
        echo($update_msg); exit;
    }

    // insert Pitch
    if(isset($_POST['pitch_name']) && isset($_POST['pitch_value']) && isset($_POST['savePitch'])){
        $name = $_POST['pitch_name'];
        $value = $_POST['pitch_value'];

        $sql = "INSERT INTO pitch (name, value) values('$name', '$value');";            
        $insert = $conn->query($sql);
        if($insert){
            $insert = "Inserted";            
        }else{
            $insert = "Could not insert!";
        }
        echo($insert); exit;
    }

    // insert System
    if(isset($_POST['system_name']) && isset($_POST['system_value']) && isset($_POST['saveSystem'])){
        $name = $_POST['system_name'];
        $value = $_POST['system_value'];
        
        $sql = "INSERT INTO system (name, value) values('$name', '$value');";            
        $insert = $conn->query($sql);
        if($insert){
            $insert = "Inserted";            
        }else{
            $insert = "Could not insert!";
        }
        echo($insert); exit;
    }

    // delete pitch
    if(isset($_POST['pitch_id']) && isset($_POST['delPitch'])){
        $id = $_POST['pitch_id'];

        $sql = "DELETE FROM pitch where id = $id;";            
        $del = $conn->query($sql);
        if($del){
            $del = "Deleted";            
        }else{
            $del = "Could not Delete!";
        }
        echo($del); exit;
    }

    // delete system
    if(isset($_POST['system_id']) && isset($_POST['delSystem'])){
        $id = $_POST['system_id'];

        $sql = "DELETE FROM system where id = $id;";            
        $del = $conn->query($sql);
        if($del){
            $del = "Deleted";            
        }else{
            $del = "Could not Delete!";
        }
        echo($del); exit;
    }

    // get all pitch    
    $stmt = $conn->prepare("SELECT * from pitch;");
    $stmt->execute();
    $pitch = $stmt->fetchAll(PDO::FETCH_OBJ);
   
    // get all systems
    $stmt = $conn->prepare("SELECT * from system;");
    $stmt->execute();
    $system = $stmt->fetchAll(PDO::FETCH_OBJ);

    // get solar size
    $stmt = $conn->prepare("SELECT * from solar;");
    $stmt->execute();
    $solar = $stmt->fetch(PDO::FETCH_OBJ);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="../assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    
    <!-- Main css -->
    <link rel="stylesheet" href="../assets/css/style.css">
  <style>


.global-container{
	height:100%;
	display: flex;
	align-items: center;
	justify-content: center;
}
.sign-up{
	text-align:center;
	padding:20px 0 0;
}

.alert{
	margin-bottom:-30px;
	font-size: 13px;
	margin-top:20px;
}
  </style>
</head>
<body>

    <div class="main">

        <?php if(!isset($_SESSION['Admin_access'])):?>
        <div class="global-container">
            <div class="card login-form">
                <div class="card-body">
                    <h3 class="card-title text-center">Admin Access</h3>
                    <hr>
                    <div class="card-text">
                        <form action="index.php" method="post">
                            <div class="form-row">
                                <div class="form-group col-md-12 text-center">
                                    <label for="accessKey">Enter Access key</label>
                                    <input type="text" class="form-control" name="accessKey" id="accessKey" placeholder="Enter Access key">
                                </div>
                                <div class="form-group col-md-12 text-center">
                                    <p class="text-warning text-center"> <?= (isset($msg)?$msg:'')?></p>
                                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                </div>
                                                               
                            </div>
                        </form>
                    
                    </div>
                </div>
            </div>
        </div>
        <?php else:?>
        <div class="container py-2">
            <h4>Setting <a href="index.php?logout=1" style="float:right;color:red;padding-right:10px;"><i class="fa fa-sign-out"></i></a></h4>
            <hr>
            <div class="row py-2">
                <div class="col-md-12">
                    <h5>Solar Capacity &nbsp;</h5>
                    <table>
                        <tr>
                            <td>
                                <input type="text" class="form-control" name="solarValue" id="solarValue" value="<?=$solar->capacity?>"/> 
                            </td>
                            <td>
                                <button class="btn btn-info btn-sm saveSolar">Save</button>                    
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    
                    <div class="table-responsive">
                        <h5>Roofing Pitch &nbsp;
                            <!-- <button class="btn btn-primary btn-sm" id="addPitch" onClick="return false;">Add</button> -->
                        </h5>
                        
                        <table class="table table-bordered table-striped table-highlight roofPitch">
                            <thead>
                                <th width="60%">Name</th>
                                <th width="20%">Value</th>
                                <th width="20%">Action</th>
                            </thead>
                            <tbody>
                                <?php foreach($pitch as $row): ?>
                                    <tr data-pitch="<?=$row->id?>">
                                        <td><input type="text" class="form-control" name="pitchName" id="pitchName" value="<?=$row->name?>"/></td>
                                        <td><input type="text" class="form-control" name="pitchValue" id="pitchValue" value="<?=$row->value?>"/></td>                                       
                                        <td>
                                            <button class="btn btn-info btn-sm editPitch">Save</button>
                                            <button class="btn btn-info btn-sm delPitch">Del</button>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                               
                                <tr>
                                    <td><input type="text" class="form-control" name="pitchName" id="pitchName" value="" placeholder="Estimated Roof Pitch"/></td>
                                    <td><input type="text" class="form-control" name="pitchValue" id="pitchValue" value="" placeholder="Pitch value"/></td>                                       
                                    <td>
                                        <button class="btn btn-info btn-sm savePitch">Add</button>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                   
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    
                    <div class="table-responsive">
                        <h5>Roofing System &nbsp;
                        <!-- <button class="btn btn-primary btn-sm" id="addSystem" onClick="return false;">Add</button>    -->
                        </h5>
                        
                        <table class="table table-bordered table-striped table-highlight roofSystem">
                            <thead>
                                <th width="60%">Name</th>
                                <th width="20%">Value</th>
                                <th width="20%">Action</th>
                            </thead>
                            <tbody>
                                <?php foreach($system as $row): ?>
                                    <tr data-system="<?=$row->id?>">
                                        <td><input type="text" class="form-control systemName" name="systemName" id="systemName"  value="<?=$row->name?>"/></td>
                                        <td><input type="text" class="form-control systemValue" name="systemValue" id="systemValue" value="<?=$row->value?>"/></td>                                       
                                        <td>
                                            <button class="btn btn-info btn-sm editSystem">Save</button>
                                            <button class="btn btn-info btn-sm delSystem">Del</button>
                                        </td>
                                    </tr>
                                <?php endforeach;?>

                                    <tr>
                                        <td><input type="text" class="form-control systemName" name="systemName"  value="" id="systemName" placeholder="Desired Roofing System"/></td>
                                        <td><input type="text" class="form-control systemValue" name="systemValue" value="" id="systemValue" placeholder="System value"/></td>                                       
                                        <td>
                                            <button class="btn btn-info btn-sm saveSystem">Add</button>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    </div>
                   
                </div>
            </div>

        </div>
        <?php endif;?>

    
    </div>
    <!-- JS -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#addPitch').on('click', function(){
                $('.roofPitch tbody tr:last').after('<tr>'+
                    '<td><input type="text" class="form-control" name="pitchName" value="" placeholder="Estimated Roof Pitch" /></td>'+
                    '<td><input type="text" class="form-control" name="pitchValue" value="" placeholder="Pitch value" /></td>'+
                    '<td><button class="btn btn-info btn-sm savePitch">Add</button>'+
                    '</td></tr>');
            });

            $('#addSystem').on('click', function(){
                $('.roofSystem tbody tr:last').after('<tr>'+
                    '<td><input type="text" class="form-control" name="systemName" value="" placeholder="Desired Roofing System" /></td>'+
                    '<td><input type="text" class="form-control" name="systemValue" value="" placeholder="System value"/></td>'+
                    '<td><button class="btn btn-info btn-sm saveSystem">Add</button>'+
                    '</td></tr>');
            });
            
            // update pitch
            $('.editPitch').on('click', function(){
                var id = $(this).closest('tr').attr('data-pitch');
                var name = $(this).closest('tr').find('#pitchName').val();
                var value = $(this).closest('tr').find('#pitchValue').val();

                sendData({'pitch_id':id,'pitch_name':name,'pitch_value':value,'updatePitch':1})
            });

            // save pitch
            $('.savePitch').on('click', function(){
                var name = $(this).closest('tr').find('#pitchName').val();
                var value = $(this).closest('tr').find('#pitchValue').val();

                sendData({'pitch_name':name,'pitch_value':value,'savePitch':1})
            });

            //delete pitch
            $('.delPitch').on('click', function(){
                var id = $(this).closest('tr').attr('data-pitch');
                
                sendData({'pitch_id':id,'delPitch':1}).then((result)=>{                    
                    if(result == 'Deleted'){
                        $(this).closest('tr').hide();
                    }
                })               
            });


            // update system
             $('.editSystem').on('click', function(){
                var id = $(this).closest('tr').attr('data-system');
                var name = $(this).closest('tr').find('#systemName').val();
                var value = $(this).closest('tr').find('#systemValue').val();

                sendData({'system_id':id,'system_name':name,'system_value':value,'updateSystem':1})
            });

            // save System
            $('.saveSystem').on('click', function(){
                var name = $(this).closest('tr').find('#systemName').val();
                var value = $(this).closest('tr').find('#systemValue').val();

                sendData({'system_name':name,'system_value':value,'saveSystem':1})
            });

            //delete System
            $('.delSystem').on('click', function(){
                var id = $(this).closest('tr').attr('data-system');
                
                sendData({'system_id':id,'delSystem':1}).then((result)=>{                    
                    if(result == 'Deleted'){
                        $(this).closest('tr').hide();
                    }
                })               
            });

            // save solar
            $('.saveSolar').on('click', function(){
                var value = $('#solarValue').val();
              
                sendData({'solar_value':value,'saveSolar':1})
            });
        });

       async function sendData(data){
            var data = await $.ajax({
                url:'index.php',
                method:'POST',
                data:data,
                success:function(result){
                    alert(result);
                    location.reload();
                }
            });
            return data;
        }
    </script>
</body>
</html>