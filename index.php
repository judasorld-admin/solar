<?php     
    include_once('config/config.php');

    // get all pitch    
    $stmt = $conn->prepare("SELECT * from pitch;");
    $stmt->execute();
    $pitch = $stmt->fetchAll(PDO::FETCH_OBJ);

    // get all systems
    $stmt = $conn->prepare("SELECT * from system;");
    $stmt->execute();
    $system = $stmt->fetchAll(PDO::FETCH_OBJ);

    // get solar size
    $stmt = $conn->prepare("SELECT * from solar;");
    $stmt->execute();
    $solar = $stmt->fetch(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up Form</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="assets/fonts/material-icon/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Main css -->
    <link rel="stylesheet" href="assets/css/style.css">
   <style>
       #map{
            width:100%;           
            height: auto;
            min-height:350px;
            overflow: hidden;
            border: none;
       }
        
   </style>
</head>
<body>

    <div class="main">

        <div class="container">
            <form method="POST" id="solar-form" class="solar-form" enctype="multipart/form-data">
                <input type="hidden" name="solar_size" id="solar_size" value="<?=$solar->capacity?>">
                <h3>
                    Property
                </h3>
                <fieldset>                    
                    <div class="form-row">
                        <div class="col-md-12">
                            <h5>Property to be Improved:</h5>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Street</label>
                            <input type="text" name="street" id="street" required="required" class="form-control" placeholder="Enter Street Name" />
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">City</label>
                            <input type="text" name="city" id="city" required="required" class="form-control" placeholder="Enter City Name" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="control-label">State</label>
                                <select class="form-control" name="state" id="state" required>
                                    <option value="">- SELECT -</option>
                                    <option value="Alabama">Alabama (AL)</option>
                                    <option value="Alaska">Alaska (AK)</option>
                                    <option value="Arizona">Arizona (AZ)</option>
                                    <option value="Arkansas">Arkansas (AR)</option>
                                    <option value="California">California (CA)</option>
                                    <option value="Colorado">Colorado (CO)</option>
                                    <option value="Connecticut">Connecticut (CT)</option>
                                    <option value="Delaware">Delaware (DE)</option>
                                    <option value="District Of Columbia">District Of Columbia (DC)</option>
                                    <option value="Florida">Florida (FL)</option>
                                    <option value="Georgia">Georgia (GA)</option>
                                    <option value="Hawaii">Hawaii (HI)</option>
                                    <option value="Idaho">Idaho (ID)</option>
                                    <option value="Illinois">Illinois (IL)</option>
                                    <option value="Indiana">Indiana (IN)</option>
                                    <option value="Iowa">Iowa (IA)</option>
                                    <option value="Kansas">Kansas (KS)</option>
                                    <option value="Kentucky">Kentucky (KY)</option>
                                    <option value="Louisiana">Louisiana (LA)</option>
                                    <option value="Maine">Maine (ME)</option>
                                    <option value="Maryland">Maryland (MD)</option>
                                    <option value="Massachusetts">Massachusetts (MA)</option>
                                    <option value="Michigan">Michigan (MI)</option>
                                    <option value="Minnesota">Minnesota (MN)</option>
                                    <option value="Mississippi">Mississippi (MS)</option>
                                    <option value="Missouri">Missouri (MO)</option>
                                    <option value="Montana">Montana (MT)</option>
                                    <option value="Nebraska">Nebraska (NE)</option>
                                    <option value="Nevada">Nevada (NV)</option>
                                    <option value="New Hampshire">New Hampshire (NH)</option>
                                    <option value="New Jersey">New Jersey (NJ)</option>
                                    <option value="New Mexico">New Mexico (NM)</option>
                                    <option value="New York">New York (NY)</option>
                                    <option value="North Carolina">North Carolina (NC)</option>
                                    <option value="North Dakota">North Dakota (ND)</option>
                                    <option value="Ohio">Ohio (OH)</option>
                                    <option value="Oklahoma">Oklahoma (OK)</option>
                                    <option value="Oregon">Oregon (OR)</option>
                                    <option value="Pennsylvania">Pennsylvania (PA)</option>
                                    <option value="Rhode Island">Rhode Island (RI)</option>
                                    <option value="South Carolina">South Carolina (SC)</option>
                                    <option value="South Dakota">South Dakota (SD)</option>
                                    <option value="Tennessee">Tennessee (TN)</option>
                                    <option value="Texas">Texas (TX)</option>
                                    <option value="Utah">Utah (UT)</option>
                                    <option value="Ver">Vermont</option>
                                    <option value="Virg">Virginia</option>
                                    <option value="Washin">Washington</option>
                                    <option value="West Virg">West Virginia</option>
                                    <option value="Wisco">Wisconsin</option>
                                    <option value="Wyo">Wyoming</option>
                                </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Zip Code</label>
                            <input type="text" name="zipcode" id="zipcode" required="required" class="form-control" placeholder="Enter Zip Code" />
                        </div>
                    </div>
                    
                   <hr>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Approximate Livable Square Feet</label>
                            <input type="text" name="total_SF" id="total_SF" required="required" class="form-control format" placeholder="Enter Roof Area" />
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Number of Stories</label>
                            <input type="number" name="stories" min="1" id="stories" value="1" required="required" class="form-control" placeholder="Number of stories" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Estimated Roof Pitch </label>
                            <select name="roof_pitch" id="roof_pitch" class="form-control" required>
                                <option value="">- SELECT -</option>
                                <?php foreach($pitch as $row): ?>
                                    <option value="<?=$row->value?>"><?=$row->name?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="control-label">Desired Roofing System</label>
                            <select name="roof_system" id="roof_system" class="form-control" required>
                                <option value="">- SELECT -</option>
                                <?php foreach($system as $row): ?>
                                    <option value="<?=$row->value?>"><?=$row->name?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>

                   
                    <div class="form-row">
                        <div class="col-md-12">
                            <h5>Energy Profile:</h5>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12 sol-sm-12">
                            <div class="form-radio">
                                <label class="control-label" for="have_solar">Do you have a solar energy system? </label>

                                <div class="form-radio-group">            
                                    <div class="form-radio-item">
                                        <input type="radio" name="have_solar" value="1" id="have_solar">
                                        <label for="have_solar">Yes</label>
                                        <span class="check"></span>
                                    </div> &nbsp;&nbsp;&nbsp;
                                    <div class="form-radio-item">
                                        <input type="radio" name="have_solar" value="0" id="no_solar">
                                        <label for="no_solar">No</label>
                                        <span class="check"></span>
                                    </div>
                                    
                                </div>
                            </div>
                                
                        </div>

                        <div class="form-group col-md-12 add_solar_box" style="display:none;">
                            <label class="control-label" for="add_solar">Do you want to add a solar system?</label>
                          
                            <div class="form-radio-group">            
                                <div class="form-radio-item">
                                    <input type="radio" name="add_solar" value="1" id="add_solar" >
                                    <label for="add_solar">Yes</label>
                                    <span class="check"></span>
                                </div>&nbsp;&nbsp;&nbsp;
                                <div class="form-radio-item">
                                    <input type="radio" name="add_solar" value="0" id="add_no_solar">
                                    <label for="add_no_solar">No</label>
                                    <span class="check"></span>
                                </div>
                                
                            </div>
                        </div> 
                    </div>
                   
                    <div class="form-row add_solar_box" style="display:none;">                   
                        <div class="form-group col-md-12 col-sm-12">
                            <label class="control-label">What is your monthly energy bill?</label>
                            <input type="text" name="energy_bill" id="energy_bill" class="form-control dollar" placeholder="Monthly bill" required/>
                        </div>                
                    </div> 
                    <br>
                </fieldset>

                <h3>
                    Financing
                </h3>
                <fieldset>
                    <div class="form-row">
                        <div class="col-md-12">
                            <h5>Current Mortgage:</h5>
                        </div>
                    </div>
                    <div class="form-row">                        
                        <div class="form-group col-md-6">
                            <label class="control-label">Amount You Owe on Your Mortgage</label>
                            <input type="text" name="home_loan" id="home_loan" class="form-control dollar" placeholder="Loan amount" required/>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Home Value</label>
                            <input type="text" name="home_value" id="home_value" class="form-control dollar" placeholder="Home value" required/>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Current Mortgage Payment </label>
                            <input type="text" name="home_repayment" id="home_repayment" class="form-control dollar" placeholder="Mortgage amount" required/>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="current_interest">Current Interest Rate %</label>
                            <input type="text" name="current_interest" id="current_interest" class="form-control percent" placeholder="Interest %" />
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label" for="current_term">Current Mortgage Term</label>                           
                            <select name="current_term" id="current_term" class="form-control" required>
                                <option value="">- SELECT -</option>
                                <option value="15">15 Years</option>
                                <option value="30">30 Years</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-12">
                            <h5>New Mortgage:</h5>
                            <p>To be funded under McCormack's M2 Program, via McCormack's partner lender.</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label class="control-label">Anticipated Mortgage Term</label>
                            <select name="term" id="term" class="form-control" required>
                                <option value="">- SELECT -</option>
                                <option value="15">15 Years</option>
                                <option value="30">30 Years</option>
                            </select>
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label class="control-label">Anticipated Interest Rate %</label>
                            <input type="text" name="interest" id="interest" class="form-control percent" placeholder="Interest %" required/>
                        </div>
                        
                    
                    </div>
                    <br>
                </fieldset>

                <h3>
                    Result
                </h3>
                <fieldset> 

                    <div class="row" id="calc_output">
                        <div class="col-md-12">
                            <h5>System Location:</h5>
                            <span class="address"></span>
                        </div>
                        <div class="col-md-12  col-sm-12">
                            <div id="map" ></div>
                        </div>
                        <div class="col-md-12 pt-2">
                            <h5>System Size and Cost:</h5>
                        </div>
                        <div class="col-md-12 col-sm-12">    
                             
                            <table class="table table-sm table-borderless">
                                <tr>
                                    <th>
                                        Anticipated new roof and solar cost: <br>
                                        (Before Tax Incentives)

                                    </th>
                                    <td><span class="solar_cost"></span></td>
                                </tr>                                
                                <tr>
                                    <th>Approximate Livable Square Feet:</th>
                                    <td><span class="total_SF format"></span></td>
                                </tr>
                                <tr>
                                    <th>Estimated Roof Square Feet:</th>
                                    <td><span class="SF_liveable format"></span></td>
                                </tr>
                                <tr>
                                    <th>Estimated Solar System Size:</th>
                                    <td><span class="solar_size"></span></td>
                                </tr>
                            </table>
                        </div>                    
                        
                        <div class="col-md-12 py-2 home_mortgage_box" style="display:none;">    
                            <div class="card">
                                <div class="card-header">
                                    <h5>Option #1</h5>
                                    <h5 class="pl-1">Finance through a New M2 Mortgage</h5>
                                </div>
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 py-2">
                                            <h5 class="card-title">BEFORE</h5>
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th>Current Mortgage:</th>
                                                    <td><span class="existing_mortgage"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Current Energy Bill:</th>
                                                    <td><span class="existing_energy_bill"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Total:</th>
                                                    <td><span class="existing_total"></span></td>
                                                </tr>                                               
                                            </table>

                                        </div>
                                        <div class="col-md-6 col-sm-12 py-2" style="background-color:#ec9117e8;">
                                            <h5 class="card-title">AFTER</h5>
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th>New Mortgage:</th>
                                                    <td><span class="new_mortgage"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>New Energy Bill:</th>
                                                    <td><span class="new_energy_bill"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Total:</th>
                                                    <td><span class="new_total"></span></td>
                                                </tr>                                               
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="card-footer text-center">                                    
                                    <h4 class="new_saving"></h4> 
                                    <h4 class="new_tax_credit"></h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 mt-4  py-2 home_improvement_box" style="display:none;">    
                            <div class="card">
                                <div class="card-header">
                                    <h5>Option #2</h5>
                                    <h5 class="pl-1">Finance through a Home Improvement Loan</h5>
                                </div>
                                <div class="card-body py-0">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 py-2">
                                            <h5 class="card-title">BEFORE</h5>
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th>Current Mortgage:</th>
                                                    <td><span class="existing_mortgage"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <th>Current Energy Bill:</th>
                                                    <td><span class="existing_energy_bill"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Total:</th>
                                                    <td><span class="existing_total"></span></td>
                                                </tr>                                               
                                            </table>
                                                                                        
                                        </div>
                                        <div class="col-md-6 col-sm-12 py-2" style="background-color:#ec9117e8;">
                                            <h5 class="card-title">AFTER</h5>
                                            <table class="table table-sm table-borderless">
                                                <tr>
                                                    <th width="65%">Current Mortgage:</th>
                                                    <td><span class="existing_mortgage"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Home Improvement Loan Payment (15 yr):</th>
                                                    <td><span class="improve_mortgage"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>New Energy Bill:</th>
                                                    <td><span class="improve_energy_bill"></span></td>
                                                </tr>
                                                <tr>
                                                    <th>Total:</th>
                                                    <td><span class="improve_total"></span></td>
                                                </tr>                                               
                                            </table>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="card-footer text-center">
                                    <h4 class="improve_saving"></h4> 
                                    <h4 class="improve_tax_credit"></h4>
                                </div>
                            </div>
                        </div>                       
                        <div class="col-md-12 py-2">
                            <em><i>*The results above are rough estimates.  To obtain more detailed sizing, pricing, and potential savings, please schedule an appointment with a McCormack estimator. </br>**Home Improvement Loan to have a 15 year term.</i></em>
                        </div>
                    </div>

                    <br>
                    <br>
                </fieldset>

                <h3>
                    Contact
                </h3>
                <fieldset>
                    <div class="form-row">
                        <div class="col-md-12">
                            <h5>Contact us:</h5>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="control-label" for="contact_name">Name</label>
                            <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Full Name" required/>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="control-label" for="contact_email">Email</label>
                            <input type="email" name="contact_email" id="contact_email" class="form-control" placeholder="Email Address" required/>
                        </div>
                        
                        <div class="form-group col-md-12">
                            <label class="control-label" for="service_request">Service Request</label>
                            <select name="service_request" id="service_request" class="form-control" required>
                                <option value="">- SELECT -</option>
                                <option value="Roof System Installation">Roof System Installation</option>
                                <option value="Roof and Solar Installation">Roof and Solar Installation</option>
                                <option value="Solar Only Installation ">Solar Only Installation </option>
                                <option value="Home Modernization Project">Home Modernization Project</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12">
                            <label class="control-label" for="want_home_improve">Are you interested in financing either through a Home Improvement Loan or a new M2 Home Modernization Mortgage?</label>
                          
                            <div class="form-radio-group">            
                                <div class="form-radio-item">
                                    <input type="radio" name="want_home_improve" value="1" id="want_home_improve" >
                                    <label for="want_home_improve">Yes</label>
                                    <span class="check"></span>
                                </div>&nbsp;&nbsp;&nbsp;
                                <div class="form-radio-item">
                                    <input type="radio" name="want_home_improve" value="0" id="want_no_home_improve">
                                    <label for="want_no_home_improve">No</label>
                                    <span class="check"></span>
                                </div>
                                
                            </div>
                        </div>     
                        
                        <div class="form-group col-md-12">
                            <p id="message_info" class="text-info"></p>
                        </div>
                    
                    </div>
                </fieldset>
            </form>
        </div>

    </div>

    <!-- JS -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="vendor/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="vendor/jquery-steps/jquery.steps.min.js"></script>
    <script async defer type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyC0qZQ6eh0-TgvnwFlbrnqzKsiY4GsyREc"></script>
    <script src="assets/js/map.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>