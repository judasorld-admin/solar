<?php
    // create table PITCH
    $sql = "CREATE TABLE IF NOT EXISTS pitch (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        value VARCHAR(100) NOT NULL
        )";
        
    $create = $conn->query($sql);

    // create table SYSTEM
    $sql = "CREATE TABLE IF NOT EXISTS system (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        value VARCHAR(100) NOT NULL
        )";
        
    $create = $conn->query($sql);

    // create table SYSTEM
    $sql = "CREATE TABLE IF NOT EXISTS solar (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        capacity VARCHAR(100) NOT NULL
        )";
        
    $create = $conn->query($sql);
?>