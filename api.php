<?php
    $data = $_POST;

    $query = http_build_query($data);

    // create curl resource
    $url = "https://developer.nrel.gov/api/pvwatts/v6?".$query;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0");   
    $result = curl_exec($ch);
    // curl_close($ch);
    if (curl_errno($ch)) {
        echo 'Error: ' . curl_error($ch);
    }
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $result = json_decode($result);

    $energy = 0;
    $month = date('m');

    if(isset($result->outputs->ac_monthly)){
        // $ac = (array)$result->outputs->ac_monthly;
        // $energy = $ac[$month -1];

        foreach($result->outputs->ac_monthly as $data){
            $energy += $data;
        }        
    }
    $rate = 0.160;
    echo json_encode([floor($energy/12), floor(($energy*$rate)/12) ]);exit;
   
?>