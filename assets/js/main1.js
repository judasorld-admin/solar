(function($) {
    var form = $("#solar-form");
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        labels: {
            previous : 'Prev',
            next : 'Next',
            finish : 'SCHEDULE',
            current : ''
        },
        titleTemplate : '<h3 class="title">#title#</h3>',
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";                      
            getEnergyBill(); 
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {            
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            alert('Save data in DB and redirect to contact page!');
        }
    });

    form.validate({                
        errorPlacement: function(error, element) { 
            var element = element.parent().find('label');
            element.css({'color':'red'});
        },
       
        highlight: function ( element) {
            var element = $(element).parent().find('label');
            element.css({'color':'black'});
        },
        unhighlight: function (element) {
            var element = $(element).parent().find('label');
            element.css({'color':'black'});
        },
        rules: {
            address: {
                required: true,
                email: true
            },
            city: {
                required: true,
                minlength: 5
            }
        }   
    });

    var input = {
        'street' :'',
        'city' : '',
        'state' : '',
        'zipcode' : '',
        'total_SF' : 0,
        'stories' : 0,                
        'roof_pitch' : 0,
        'roof_system': 0,
        'have_solar': 0,
        'add_solar' : 0,
        'energy_bill' : 0,
        'home_value':500000,
        'home_loan' : 0,
        'home_repayment':0,
        'mortgage_frequency':'',
        'current_interest':4,
        'current_term' : 30,
        'interest':3,
        'term' : 30,
    };
   
    var output = {
        'SF_floor' : 0,
        'SF_liveable' : 0,
        'installation_cost' : 0,
        'solar_size':5,
        'new_finance':{
            'energy_bill':0,
            'loan':0,
            'mortgage' : 0,
            'saving':0,
            'tax_credit': 0             
        },
        'home_improve':{
            'energy_bill':0,
            'loan':0,
            'mortgage' : 0,
            'saving':0,
            'tax_credit': 0             
        },
    };

    // chnage in input for anticipated int and term
    $('#term').change(function(){ 
        var value = parseInt($(this).val());
        var int = 3.25;
        if(value == 15){
            int = 2.54;
        }
        if(value == 30){
            int = 3.04;
        }

        $('#interest').val(formatCurrency(int,'%'));
    })

    // calculate output on form change        
    $('#solar-form .form-control').change(function(){
        // set input
        $('#solar-form .form-control').each(function(){
            var name = $(this).attr('name');
            var value = $(this).val();
            input[name] = value;          
        })           
        
        sanitizeinput();

        if(input.street != '' && input.state  != ''  && input.state != '' && input.zipcode != ''){
            initialize(input);
        }
       
        if(input.total_SF && input.stories && input.roof_pitch && input.roof_system){
            output.SF_floor = (input.total_SF / input.stories);              
        }
        if(output.SF_floor > 0 ){
            output.SF_liveable = output.SF_floor*input.roof_pitch;               
        }
        if(output.SF_liveable > 0 ){
            // installation cost
            output.installation_cost = output.SF_liveable * input.roof_system + output.solar_size*4000;
            
            output.new_finance.loan = input.home_loan + output.installation_cost;                                  
            output.home_improve.loan = output.installation_cost*1.11;

            if(output.new_finance.loan > 0 && input.interest > 0 && input.term != 0){
                var repayment = getPILoan(input.term,input.interest,12,output.new_finance.loan);
                output.new_finance.mortgage = parseFloat(repayment); 

                // var repayment2 = getPILoan(input.term,input.interest,12,output.home_improve.loan);
                var repayment2 = getPILoan(15,7,12,output.home_improve.loan);
                output.home_improve.mortgage = parseFloat(repayment2); 

                setOutput();
            }
        }        
    });
    
    $('input[name="have_solar"]').change(function(){ 
        var value = $(this).val();
        input.have_solar = parseInt(value);
        
        if(value == 0){
            $('.add_solar_box').show();
        }else{
            $('.add_solar_box').hide();
        }
        
    })

    $('input[name="add_solar"]').change(function(){ 
        var value = $(this).val();
        input.add_solar = parseInt(value);
    })
    
    function setOutput(){
        // console.table(input)
        // console.table(output)

        $('.home_mortgage_box').show();
        $('.home_improvement_box').show();
        
        $('#calc_output .solar_cost').text(formatCurrency(Math.round(output.installation_cost)));
        $('#calc_output .address').text((input.street +' '+input.city +', '+input.state +' '+ input.zipcode ));
        $('#calc_output .total_SF').text(numberFormat(Math.round(input.total_SF)));
        $('#calc_output .SF_liveable').text(numberFormat(Math.round(output.SF_liveable)));
        $('#calc_output .solar_size').text(output.solar_size+' KW');
        $('#calc_output .improve_loan').text(formatCurrency(output.home_improve.loan));
        $('#calc_output .interest').text(formatCurrency(input.interest,'%'));
        $('#calc_output .term').text(input.term);

        $('#calc_output .existing_mortgage').text(formatCurrency(input.home_repayment)+ ' /m');
        $('#calc_output .existing_energy_bill').text(formatCurrency(input.energy_bill)+ ' /m');
        var total1 = input.home_repayment + input.energy_bill;       
        $('#calc_output .existing_total').text(formatCurrency(total1)+ ' /m');

        $('#calc_output .new_mortgage').text(formatCurrency(output.new_finance.mortgage)+ ' /m');
        $('#calc_output .new_energy_bill').text(formatCurrency(output.new_finance.energy_bill)+ ' /m');
        var total2 = output.new_finance.mortgage + output.new_finance.energy_bill       
        $('#calc_output .new_total').text(formatCurrency(total2)+ ' /m');

        output.new_finance.saving = total1 - total2;
        output.new_finance.tax_credit = output.solar_size * 4000 *0.26;

        if(output.new_finance.saving > 0){
            $('#calc_output .new_saving').html('<p style="color:green;">Net Monthly Savings: <b>'+formatCurrency(output.new_finance.saving)+'</p>');
            
        }else{
            $('#calc_output .new_saving').html('<p style="color:red;">Net Monthly Cost: <b>'+formatCurrency(output.new_finance.saving)+'</b></p>');
        }
        $('#calc_output .new_tax_credit').html('<p style="color:green;">Tax Credit You Receive in Y1: <b>'+formatCurrency(output.new_finance.tax_credit)+'</b></p>');


        // home improvement
        $('#calc_output .improve_mortgage').text(formatCurrency(output.home_improve.mortgage)+ ' /m');
        $('#calc_output .improve_energy_bill').text(formatCurrency(output.home_improve.energy_bill)+ ' /m');
        var total2 = output.home_improve.mortgage + output.home_improve.energy_bill + input.home_repayment;      
        $('#calc_output .improve_total').text(formatCurrency(total2)+ ' /m');

        output.home_improve.saving = total1 - total2;
        output.home_improve.tax_credit = output.solar_size * 4000 *0.26;

        if(output.home_improve.saving > 0){
            $('#calc_output .improve_saving').html('<p style="color:green;">Net Monthly Cost / Savings: <b>'+formatCurrency(output.home_improve.saving)+'</p>');
            
        }else{
            $('#calc_output .improve_saving').html('<p style="color:red;">Net Monthly Cost: <b>'+formatCurrency(output.home_improve.saving)+'</b></p>');
        }
        $('#calc_output .improve_tax_credit').html('<p style="color:green;">Tax Credit You Receive in Y1: <b>'+formatCurrency(output.home_improve.tax_credit)+'</b></p>');

    }

    function sanitizeinput(){
        input = {            
                'street' :input.street,
                'city' : input.city,
                'state' : input.state,        
                'zipcode' : parseFloat(input.zipcode),
                'total_SF' : parseFloat(unformatCurrency(input.total_SF)),
                'stories' : parseFloat(input.stories),  
                'roof_pitch' : parseFloat(input.roof_pitch),
                'roof_system': parseFloat(input.roof_system),
                'have_solar': parseFloat(input.have_solar),
                'add_solar' : parseFloat(input.add_solar),
                'energy_bill' : unformatCurrency(input.energy_bill),
                'home_value':unformatCurrency(input.home_value),
                'home_loan' : unformatCurrency(input.home_loan),                
                'home_repayment': unformatCurrency(input.home_repayment),
                'current_interest': unformatCurrency(input.current_interest),
                'current_term' : parseFloat(input.current_term),
                'interest': unformatCurrency(input.interest),
                'term' : parseFloat(input.term)
        };
    }

    async function  getEnergyBill(){
        var data = {
            'format':'json',
            'system_capacity' : 5,
            'module_type' : 0,
            'losses' : 14,
            'array_type' : 0,
            'tilt' : 20,
            'azimuth' : 180,
            'address' : input.street +' '+input.city +', '+input.state +' '+ input.zipcode + ' United States',
            // 'lat' => 48.40204,
            // 'lon' => 16.14009,
            'dataset' :'nsrdb',
            'api_key' : 'VS7oJ7xKcvBSo6axDzlQYk76SBXY4QYS9Fi7GkLk'
        };
        var bill = 0;

        await $.ajax({
            url:"api.php",
            method: "POST",
            data: data,
            success:function( data ) {
                var result = JSON.parse(data);
                bill = parseInt(result[1]);
    
            },
            error:function( jqXHR, textStatus ) {
                console.log(data)
            }
        });          
       
        bill  = (bill >1)? bill: 500;
        output.new_finance.energy_bill = input.energy_bill - bill;
        output.home_improve.energy_bill = output.new_finance.energy_bill;
    }
    
    // currency format
    function formatCurrency(num, sin= '$')
    {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
        {
            num = "0";
        }

        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();

        if (cents < 10)
        {
            cents = "0" + cents;
        }
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        {
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        }    
        if(sin === '$')
            return (((sign) ? '' : '-') + sin + num );
        
        if(sin === '%')
            return (((sign) ? '' : '-') + num + '.' + cents + sin);
            
    }

    // number format
    function numberFormat(num){
        return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    // unformat currency
    function unformatCurrency(value){
        return Number(value.replace(/[^0-9.-]+/g,""));
    }
    //convert to dollar format
    document.querySelectorAll('.dollar').forEach(item => {
        item.addEventListener('change', event => {
            item.value = formatCurrency(item.value);
        })
        // item.value = formatCurrency(item.value);
    })
    
    //convert to dollar format
    document.querySelectorAll('.format').forEach(item => {
        item.addEventListener('change', event => {
            item.value = numberFormat(item.value);
        })
        // item.value = formatCurrency(item.value);
    })

    //convert to percentage
    document.querySelectorAll('.percent').forEach(item => {
        item.addEventListener('change', event => {
            var max = item.getAttribute('max'); 
            var limit = 100;

            if(max){
                limit = max;
            }

            if(item.value <= limit ){
                item.value = formatCurrency(item.value, '%');
            }
            else{
                item.value = '100.00%';
            }
            
        })
        // item.value = formatCurrency(item.value, '%');
    })

    function getPILoan(term,intrs,frequency,loan){
        intrs = parseFloat(intrs) / 100 / 12;

        var period = parseInt(frequency) * parseInt(term);		  

        // pmt returns negative number 
        var pmt_payment = parseFloat(pmt(intrs, period, loan, 0,0)) * -1;		 			
        
        return pmt_payment.toFixed(2);
    }

    function pmt(rate_per_period, number_of_payments, present_value, future_value, type){
        if(rate_per_period != 0.0){
            // Interest rate exists
            var q = Math.pow(1 + rate_per_period, number_of_payments);
            return -(rate_per_period * (future_value + (q * present_value))) / ((-1 + q) * (1 + rate_per_period * (type)));

        } else if(number_of_payments != 0.0){
            // No interest rate, but number of payments exists
            return -(future_value + present_value) / number_of_payments;
        }

        return 0;
    }


})(jQuery);

