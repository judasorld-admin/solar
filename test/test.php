<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style>
        /* Latest compiled and minified CSS included as External Resource*/

/* Optional theme */
#0176bc

#f26436
/*@import url('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css');*/
 body {
    margin-top:30px;
}
.panel-primary>.panel-heading{
    background-color: #0176bc !important;;
    border-color:#017600 !important;
}

.has-error .help-block, .has-error .control-label{
    color:#f26436 !important;
}
.btn-primary{
    background-color:#0176bc !important;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#0176bc;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#f26436;
}
.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content:" ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-index: 0;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
#map {
    width: 250px;
    height: 250px;
    background-color: grey;
    }

    .result-text{
        color: #f26436;
    }
    
    </style>
</head>
<!-- onload="initialize()" -->
<body >

<div class="container">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                <p><small>Basic Detail</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Finance</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Result</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Enquire</small></p>
            </div>
        </div>
    </div>
    
    <form role="form" id="solar-form">
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                 <h3 class="panel-title">Address & Roof Detail</h3>
            </div>
            <div class="panel-body">
                <h4>Address:</h4>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Street</label>
                        <input type="text" name="street" id="street" required="required" class="form-control" placeholder="Enter Street Name" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">City</label>
                        <input type="text" name="city" id="city" required="required" class="form-control" placeholder="Enter City Name" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">State</label>
                            <select class="form-control" name="state" id="state" required>
                                <option value="">- SELECT -</option>
                                <option value="AL">Alabama (AL)</option>
                                <option value="AK">Alaska (AK)</option>
                                <option value="AZ">Arizona (AZ)</option>
                                <option value="AR">Arkansas (AR)</option>
                                <option value="CA">California (CA)</option>
                                <option value="CO">Colorado (CO)</option>
                                <option value="CT">Connecticut (CT)</option>
                                <option value="DE">Delaware (DE)</option>
                                <option value="DC">District Of Columbia (DC)</option>
                                <option value="FL">Florida (FL)</option>
                                <option value="GA">Georgia (GA)</option>
                                <option value="HI">Hawaii (HI)</option>
                                <option value="ID">Idaho (ID)</option>
                                <option value="IL">Illinois (IL)</option>
                                <option value="IN">Indiana (IN)</option>
                                <option value="IA">Iowa (IA)</option>
                                <option value="KS">Kansas (KS)</option>
                                <option value="KY">Kentucky (KY)</option>
                                <option value="LA">Louisiana (LA)</option>
                                <option value="ME">Maine (ME)</option>
                                <option value="MD">Maryland (MD)</option>
                                <option value="MA">Massachusetts (MA)</option>
                                <option value="MI">Michigan (MI)</option>
                                <option value="MN">Minnesota (MN)</option>
                                <option value="MS">Mississippi (MS)</option>
                                <option value="MO">Missouri (MO)</option>
                                <option value="MT">Montana (MT)</option>
                                <option value="NE">Nebraska (NE)</option>
                                <option value="NV">Nevada (NV)</option>
                                <option value="NH">New Hampshire (NH)</option>
                                <option value="NJ">New Jersey (NJ)</option>
                                <option value="NM">New Mexico (NM)</option>
                                <option value="NY">New York (NY)</option>
                                <option value="NC">North Carolina (NC)</option>
                                <option value="ND">North Dakota (ND)</option>
                                <option value="OH">Ohio (OH)</option>
                                <option value="OK">Oklahoma (OK)</option>
                                <option value="OR">Oregon (OR)</option>
                                <option value="PA">Pennsylvania (PA)</option>
                                <option value="RI">Rhode Island (RI)</option>
                                <option value="SC">South Carolina (SC)</option>
                                <option value="SD">South Dakota (SD)</option>
                                <option value="TN">Tennessee (TN)</option>
                                <option value="TX">Texas (TX)</option>
                                <option value="UT">Utah (UT)</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Zip code</label>
                        <input type="text" name="zipcode" id="zipcode" required="required" class="form-control" placeholder="Enter Zip Code" />
                    </div>
                </div>
                <hr>
                <h4>Roof Detail:</h4>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Approximate Livable Square Footage</label>
                        <input type="text" name="total_SF" id="total_SF" required="required" class="form-control" placeholder="Enter Roof Area" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Number of Stories</label>
                        <input type="text" name="stories" min="1" id="stories" value="1" required="required" class="form-control" placeholder="Enter City Name" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Estimated Roof Pitch </label>
                        <select name="roof_pitch" id="roof_pitch" class="form-control" required>
                            <option value="">- SELECT -</option>
                            <option value="1.25">Standard- Flat to 6" per one foot in length</option>
                            <option value="1.5">Higher- 7" to 9" per one foot </option>
                            <option value="2">Highest- 10" to 12" per one foot</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Desired Roofing System</label>
                        <select name="roof_system" id="roof_system" class="form-control" required>
                            <option value="">- SELECT -</option>
                            <option value="68">Stone Coated Steel Tile</option>
                            <option value="78">Concrete or Clay Tile</option>
                            <option value="150">Slate</option>
                            <option value="58">Composite Shingle</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group col-md-6 sol-sm-12">
                        <label class="control-label">Do you already have a solar energy system? </label>
                        <select name="have_solar" id="have_solar" class="form-control" required>
                            <option value="">- SELECT -</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
                <div class="form-row">                   
                    <div class="form-group col-md-6">
                        <label class="control-label">Do you want to add a solar system to your roofing project?</label>
                        <select name="add_solar" id="add_solar" class="form-control" required>
                            <option value="">- SELECT -</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>                   
                </div>
                <div class="form-row">                   
                    <div class="form-group col-md-6 col-sm-12">
                        <label class="control-label">Monthly Energy Bill $</label>
                        <input type="text" name="energy_bill" id="energy_bill" class="form-control" placeholder="Monthly bill" />
                    </div>                
                </div>

                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                 <h3 class="panel-title">Finance</h3>
            </div>
            <div class="panel-body">
                <h4>Existing Finance:</h4>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Approximate Home Value</label>
                        <input type="text" name="home_value" id="home_value" class="form-control" placeholder="Home value" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Amount You Owe on Your Mortgage</label>
                        <input type="text" name="home_loan" id="home_loan" class="form-control" placeholder="Loan amount" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Current Mortgage Payment </label>
                        <input type="text" name="current_mortgage" id="current_mortgage" class="form-control" placeholder="Mortgage amount" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Frequency</label>
                        <select name="mortgage_frequency" id="mortgage_frequency" class="form-control">
                            <option value="">- SELECT -</option>
                            <option value="52">Weekly</option>
                            <option value="26">Fortnightly</option>
                            <option value="12" selected>Monthly</option>
                            <option value="1" >Yearly</option>
                        </select>
                    </div>
                </div>
                <h4>New Finance:</h4>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="control-label">Desired Interest Rate %</label>
                        <input type="text" name="interest" id="interest" class="form-control" placeholder="Interest %" />
                    </div>
                    <div class="form-group col-md-6">
                        <label class="control-label">Desired Mortgage Term</label>
                        <select name="term" id="term" class="form-control">
                            <option value="">- SELECT -</option>
                            <option value="52">15 Years</option>
                            <option value="26">30 Years</option>
                        </select>
                    </div>
                   
                </div>
                <button class="btn btn-primary nextBtn pull-right" type="button">Submit</button>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                 <h3 class="panel-title">Result</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <h4>PROPERTY DETAIL</h4>
                        <table class="table">
                            <tr>
                                <th>Address</th>
                                <td class="address">24 Wonga Street Canterbury Australia</td>
                            </tr>
                            <tr>
                                <th>Estimated Roof Square Footage</th>
                                <td class="total_SF">250</td>
                            </tr>
                            <tr>
                                <th>Estimated Solar System Size</th>
                                <td class="total_SF">5 KW</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <div id="map" ></div>
                    </div>
                    
                    <div class="col-md-12" style="background-color:#f5f5f5;">
                        <h4>FINANCE THROUGH NEW MORTGAGE</h4>
                        <div class="row">
                            <div class="col-md-6">                            
                                <table class="table">
                                    <tr>
                                        <th>Existing Mortgage Payment</th>
                                        <td class="total_SF">$500,000</td>
                                    </tr>
                                    <tr>
                                        <th>Current Energy Bill</th>
                                        <td class="total_SF">$450</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td class="total_SF">$4500</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">                               
                                <table class="table">
                                    <tr>
                                        <th>New Mortgage Payment</th>
                                        <td class="total_SF result-text">$500,000</td>
                                    </tr>
                                    <tr>
                                        <th>New Energy Bill</th>
                                        <td class="total_SF result-text">$450</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td class="total_SF result-text">$4500</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12 result-text">  
                                <h5>Net Monthly Cost or Savings: $5000</h5>
                                <h5>Tax Credit You Receive in Y1: $100</h5>
                            </div>
                        </div>
                    </div>

                     <div class="col-md-12 mt-4" >
                        <h4>FINANCE THROUGH SEPARATE HOME IMPROVEMENT LOAN</h4>
                        <div class="row">
                            <div class="col-md-6">                            
                                <table class="table">
                                    <tr>
                                        <th>Current Energy Bill</th>
                                        <td class="total_SF">$500,000</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td class="total_SF">$450</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">                                
                                <table class="table result-text">
                                    <tr>
                                        <th>New Energy Bill</th>
                                        <td class="total_SF result-text">$500,000</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td class="total_SF result-text">$450</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-12 result-text">  
                                <h5>Net Monthly Cost or Savings: $5000</h5>
                                <h5>Tax Credit You Receive in Y1: $100</h5>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <button class="btn btn-primary pull-right" type="button">Enquire</button>
            </div>
        </div>
        
        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                 <h3 class="panel-title">Enquiry</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Full Name" />
                </div>
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input maxlength="200" type="email" required="required" class="form-control" placeholder="Enter Email" />
                </div>
                <div class="form-group">
                    <label class="control-label">Contact</label>
                    <input maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Contact" />
                </div>

                <button class="btn btn-success pull-right" type="Submit">Enquire</button>
            </div>
        </div>
    </form>
</div>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<script async defer type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAy7MKqv3jGs9oKICY8ZWVXNFABFAMMAx8"></script>

<script src="assets/js/map.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>
<Script>

    $(document).ready(function () {

        var input = {
                'street' :'',
                'city' : '',
                'state' : '',
                'zipcode' : '',
                'total_SF' : 0,
                'stories' : 0,                
                'roof_pitch' : 0,
                'roof_system': 0,
                'have_solar': 0,
                'add_solar' : 0,
                'energy_bill' : 0,
                'home_value':500000,
                'home_loan' : 0,
                'mortgage_frequency':'',
                'current_mortgage':0,
                'interest':3,
                'term' : 30,
            };
        
        var output = {
            'SF_floor' : 0,
            'SF_liveable' : 0,
            'installation_cost' : 0,
            'solar_size':0,
            'existing_finance':{
                'energy_bill':0,
                'mortgage' : 0,
                'saving':0,
                'tax_credit': 0
            },
            'new_finance':{
                'energy_bill':0,
                'mortgage' : 0,
                'saving':0,
                'tax_credit': 0             
            },
        };

        // calculate output on form change        
        $('#solar-form .form-control').change(function(){
            // set input
            $('#solar-form .form-control').each(function(){
                var name = $(this).attr('name');
                var value = $(this).val();
                input[name] = value;
            })           
            sanitizeinput();
            console.log(input);
                      
            if(input.total_SF != 0 && input.stories != 0 && input.roof_pitch != 0 && input.roof_system != 0){
                output.SF_floor = (input.total_SF / input.stories);                
            }
            if(output.SF_floor > 0 ){
                output.SF_liveable = output.SF_floor*input.roof_pitch;               
            }
            if(output.SF_liveable > 0 ){
                output.installation_cost = output.SF_liveable * input.roof_system;
            }
            console.log(output);
        });




        // wizard 
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-success').addClass('btn-default');
                $item.addClass('btn-success');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url']"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
        });
        

        $('div.setup-panel div a.btn-success').trigger('click');
        
        var parameters = 'format=json&system_capacity=1000&module_type=0&losses=5&array_type=1&tilt=15&azimuth=90';
        
        var request = $.ajax({
            url:"api.php",
            method: "GET",          
        });
            
        request.done(function( data ) {
            console.log(data)
        });
        
        request.fail(function( jqXHR, textStatus ) {
            console.log(data)
        });

       function sanitizeinput(){
            input = {            
                    'street' :input.street,
                    'city' : input.city,
                    'state' : input.state,        
                    'zipcode' : parseFloat(input.zipcode),
                    'total_SF' : parseFloat(input.total_SF),
                    'stories' : parseFloat(input.stories),  
                    'roof_pitch' : parseFloat(input.roof_pitch),
                    'roof_system': parseFloat(input.roof_system),
                    'have_solar': parseFloat(input.have_solar),
                    'add_solar' : parseFloat(input.add_solar),
                    'energy_bill' : parseFloat(input.energy_bill),
                    'home_value':parseFloat(input.home_value),
                    'home_loan' : parseFloat(input.home_loan),
                    'mortgage_frequency': parseFloat(input.mortgage_frequency),
                    'current_mortgage': parseFloat(input.current_mortgage),
                    'interest': parseFloat(input.interest),
                    'term' : parseFloat(input.term)
            };
       }

});
</Script>

</body>
</html>